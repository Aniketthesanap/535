import torch
import os
import numpy as np
import sys
import json
import lmdb

from torch import nn
from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image
from tqdm import tqdm
from io import BytesIO


mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

def get_train_transforms(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
    return transforms.Compose([
        transforms.Resize(256),
        transforms.ColorJitter(hue=.05, saturation=.05),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.RandomRotation(20, resample=Image.BILINEAR),
        transforms.RandomCrop((224,224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ])

def get_val_transforms(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
    return transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop((224,224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ])


class Pizza10Dataset(Dataset):
    def __init__(
        self, 
        lmdb_file=f'/common/home/as3503/as3503/courses/cs535/535/dataset/Pizza10/data.lmdb', 
        part='train', transform=None, resolution=256, return_image=True):

        dirname = os.path.dirname(lmdb_file)
        categories_file = os.path.join(dirname, 'categories.txt')
        label_file = os.path.join(dirname, 'imageLabels.txt')
        with open(label_file, 'r') as f:
            self.labels = f.read().strip().split('\n')

        with open(categories_file) as f:
            self.categories = f.read().strip().split('\n')
            self.categories_mapping = {category:idx for idx, category in enumerate(self.categories)}
            self.reverse_categories_mapping = {v:k for k, v in self.categories_mapping.items()}
            
        label_dict= {}
        for idx,line in enumerate(self.labels):
            if not line in label_dict:
                label_dict[line] = []
            label_dict[line].append(idx)
        
        items = label_dict.items()
        items = sorted(items, key=lambda x: -len(x[1]))
        
        filename = os.path.join(dirname, 'statistics.json')
        with open(filename, 'w') as f:
            json.dump(items, f, indent=2)
        
        self.label_list = list(label_dict.values())
        self.class_sample_count = [len(x) for x in self.label_list]
        
        self.env = lmdb.open(
            lmdb_file,
            max_readers=32,
            readonly=True,
            lock=False,
            readahead=False,
            meminit=False,
        )

        if not self.env:
            raise IOError('Cannot open lmdb dataset', lmdb_file)

        with self.env.begin(write=False) as txn:
            self.length = int(txn.get('length'.encode('utf-8')).decode('utf-8'))

        self.resolution = resolution

        if transform is None:
            if part == 'train':
                transform = get_train_transforms()
            else:
                transform = get_val_transforms()
        
        self.transform = transform

        self.part = part

        self.return_image = return_image

    def __len__(self):
        return len(self.label_list)

    def _load_pizza(self, idx):
        with self.env.begin(write=False) as txn:
            key = f'{idx}'.encode('utf-8')
            ingrs = txn.get(key).decode('utf-8')
            if not ingrs:
                ingrs = 'empty'
            txt = ingrs

            key = f'{self.resolution}-{idx}'.encode('utf-8')
            img_bytes = txn.get(key)
        
        buffer = BytesIO(img_bytes)
        img = Image.open(buffer)
        img = self.transform(img)
        return txt, img

    def _get_categories_tensor(self, txt):
        categories = txt.split('\n')
        categories_list = []
        for category in self.categories:
            categories_list.append(int(category in categories))
        return torch.tensor(categories_list)
            
    def __getitem__(self, idx):
        pizzas = self.label_list[idx]
        if self.part=="train":
            index = pizzas[int(3*np.random.random()/5*len(pizzas))]
        else:
            index = pizzas[int(3*len(pizzas)/5)+int(2*np.random.random()*len(pizzas)/5)]
        txt, img = self._load_pizza(index)

        if not self.return_image:
            return txt

        return self._get_categories_tensor(txt).float(), img