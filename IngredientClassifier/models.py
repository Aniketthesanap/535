import torch

from torch import nn
from torch.nn import functional as F
from torchvision import models


class IngredientClassifier(nn.Module):
    def __init__(self, num_ingredients=10):
        super().__init__()
        self.main = models.resnet50(pretrained=True)
        num_feat = self.main.fc.in_features
        self.main.fc = nn.Linear(num_feat, num_ingredients)

    def forward(self, img):
        return self.main(img)