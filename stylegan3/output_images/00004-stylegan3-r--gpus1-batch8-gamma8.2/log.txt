Loading training set...

Num images:  18430
Image shape: [3, 1080, 1080]
Label shape: [0]

Constructing networks...
Setting up PyTorch plugin "bias_act_plugin"... Done.
Setting up PyTorch plugin "filtered_lrelu_plugin"... Done.
Traceback (most recent call last):
  File "/data/local/as3503/535/stylegan3/train.py", line 286, in <module>
    main() # pylint: disable=no-value-for-parameter
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1128, in __call__
    return self.main(*args, **kwargs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1053, in main
    rv = self.invoke(ctx)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1395, in invoke
    return ctx.invoke(self.callback, **ctx.params)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 754, in invoke
    return __callback(*args, **kwargs)
  File "/data/local/as3503/535/stylegan3/train.py", line 281, in main
    launch_training(c=c, desc=desc, outdir=opts.outdir, dry_run=opts.dry_run)
  File "/data/local/as3503/535/stylegan3/train.py", line 96, in launch_training
    subprocess_fn(rank=0, c=c, temp_dir=temp_dir)
  File "/data/local/as3503/535/stylegan3/train.py", line 47, in subprocess_fn
    training_loop.training_loop(rank=rank, **c)
  File "/data/local/as3503/535/stylegan3/training/training_loop.py", line 168, in training_loop
    img = misc.print_module_summary(G, [z, c])
  File "/data/local/as3503/535/stylegan3/torch_utils/misc.py", line 216, in print_module_summary
    outputs = module(*inputs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/nn/modules/module.py", line 1071, in _call_impl
    result = forward_call(*input, **kwargs)
  File "/data/local/as3503/535/stylegan3/training/networks_stylegan3.py", line 512, in forward
    img = self.synthesis(ws, update_emas=update_emas, **synthesis_kwargs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/nn/modules/module.py", line 1071, in _call_impl
    result = forward_call(*input, **kwargs)
  File "/data/local/as3503/535/stylegan3/training/networks_stylegan3.py", line 471, in forward
    x = getattr(self, name)(x, w, **layer_kwargs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/nn/modules/module.py", line 1071, in _call_impl
    result = forward_call(*input, **kwargs)
  File "/data/local/as3503/535/stylegan3/training/networks_stylegan3.py", line 355, in forward
    x = filtered_lrelu.filtered_lrelu(x=x, fu=self.up_filter, fd=self.down_filter, b=self.bias.to(x.dtype),
  File "/data/local/as3503/535/stylegan3/torch_utils/ops/filtered_lrelu.py", line 115, in filtered_lrelu
    return _filtered_lrelu_cuda(up=up, down=down, padding=padding, gain=gain, slope=slope, clamp=clamp, flip_filter=flip_filter).apply(x, fu, fd, b, None, 0, 0)
  File "/data/local/as3503/535/stylegan3/torch_utils/ops/filtered_lrelu.py", line 217, in forward
    y, so, return_code = _plugin.filtered_lrelu(x, fu, fd, b, si, up, down, px0, px1, py0, py1, sx, sy, gain, slope, clamp, flip_filter, write_signs)
RuntimeError: CUDA out of memory. Tried to allocate 3.89 GiB (GPU 0; 15.74 GiB total capacity; 8.64 GiB already allocated; 3.59 GiB free; 10.80 GiB reserved in total by PyTorch)
