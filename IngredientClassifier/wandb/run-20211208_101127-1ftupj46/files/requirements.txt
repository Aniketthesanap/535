antlr4-python3-runtime==4.8
argon2-cffi==20.1.0
astroid==2.5.6
async-generator==1.10
attrs==21.2.0
backcall==0.2.0
beautifulsoup4==4.10.0
bleach==3.3.0
certifi==2021.5.30
cffi==1.14.5
chardet==4.0.0
click==8.0.1
configparser==5.0.2
cycler==0.10.0
cython==0.29.23
dataclasses==0.6
decorator==5.0.9
defusedxml==0.7.1
docker-pycreds==0.4.0
einops==0.3.0
entrypoints==0.3
fairseq==0.10.2
fastbpe==0.1.0
filelock==3.0.12
future==0.18.2
gdown==4.2.0
gensim==4.0.1
gitdb==4.0.7
gitpython==3.1.17
huggingface-hub==0.0.8
hydra-core==1.1.0
idna==2.10
importlib-resources==5.1.4
ipykernel==5.3.4
ipython-genutils==0.2.0
ipython==7.22.0
ipywidgets==7.6.3
isort==5.8.0
jedi==0.17.0
jinja2==3.0.1
joblib==1.0.1
jsonschema==3.2.0
jupyter-client==6.1.12
jupyter-core==4.7.1
jupyterlab-pygments==0.1.2
jupyterlab-widgets==1.0.0
kiwisolver==1.3.1
lazy-object-proxy==1.6.0
lmdb==0.99
markupsafe==2.0.1
matplotlib==3.2.2
mccabe==0.6.1
mistune==0.8.4
more-itertools==8.8.0
nbclient==0.5.3
nbconvert==6.0.7
nbformat==5.1.3
nest-asyncio==1.5.1
nltk==3.5
notebook==6.4.0
numpy==1.18.5
omegaconf==2.1.0
opencv-python==4.4.0.46
packaging==20.9
pandas==1.0.5
pandocfilters==1.4.3
parso==0.8.2
pexpect==4.8.0
pickleshare==0.7.5
pillow==7.2.0
pip==21.1.1
pluggy==0.13.1
portalocker==2.0.0
prometheus-client==0.11.0
promise==2.3
prompt-toolkit==3.0.17
protobuf==3.17.1
psutil==5.8.0
ptyprocess==0.7.0
py==1.10.0
pycparser==2.20
pygments==2.9.0
pylint==2.8.3
pyparsing==2.4.7
pyrsistent==0.17.3
pysocks==1.7.1
pytest==5.4.3
python-dateutil==2.8.1
pytz==2021.1
pyyaml==5.4.1
pyzmq==20.0.0
regex==2021.4.4
requests==2.25.1
sacrebleu==1.5.1
sacremoses==0.0.45
scikit-learn==0.23.1
scipy==1.5.0
seaborn==0.11.1
send2trash==1.5.0
sentencepiece==0.1.95
sentry-sdk==1.1.0
setuptools==52.0.0.post20210125
shortuuid==1.0.1
six==1.16.0
sklearn==0.0
smart-open==5.1.0
smmap==4.0.0
soupsieve==2.3.1
subprocess32==3.5.4
subword-nmt==0.3.7
terminado==0.10.0
testpath==0.5.0
threadpoolctl==2.1.0
tokenizers==0.10.3
toml==0.10.2
torch==1.6.0
torchvision==0.7.0
tornado==6.1
tqdm==4.47.0
traitlets==5.0.5
transformers==4.7.0
urllib3==1.26.5
wandb==0.10.11
watchdog==2.1.2
wcwidth==0.2.5
webencodings==0.5.1
wheel==0.36.2
widgetsnbextension==3.5.1
wrapt==1.12.1
zipp==3.4.1