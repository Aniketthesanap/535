import torch
import wandb
import os
import numpy as np
import argparse
import sys
import json
import lmdb
import random
import pickle

from torch import nn
from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image
from sklearn.metrics import precision_score, recall_score, f1_score
from tqdm import tqdm
from io import BytesIO
from dataset import Pizza10Dataset
from models import IngredientClassifier


def calculate_metrics(pred, target, threshold=0.5):
    with torch.no_grad():
        pred = torch.sigmoid(pred)
    pred = np.array(pred.detach().cpu() > threshold, dtype=float)
    target = target.cpu().numpy()
    return {
        'micro/precision': precision_score(y_true=target, y_pred=pred, average='micro'),
        'micro/recall': recall_score(y_true=target, y_pred=pred, average='micro'),
        'micro/f1': f1_score(y_true=target, y_pred=pred, average='micro'),
        'macro/precision': precision_score(y_true=target, y_pred=pred, average='macro'),
        'macro/recall': recall_score(y_true=target, y_pred=pred, average='macro'),
        'macro/f1': f1_score(y_true=target, y_pred=pred, average='macro'),
        'samples/precision': precision_score(y_true=target, y_pred=pred, average='samples'),
        'samples/recall': recall_score(y_true=target, y_pred=pred, average='samples'),
        'samples/f1': f1_score(y_true=target, y_pred=pred, average='samples'),
    }

def my_collate(batch):
    batch = list(filter(lambda x : x[1] is not None, batch))
    return torch.utils.data.dataloader.default_collate(batch)

def train(model, train_data, val_data, args):
    criterion = torch.nn.BCEWithLogitsLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)


    for epoch in range(args.epochs):

        train_metrics = {
            'train_micro/precision': 0,
            'train_micro/recall': 0,
            'train_micro/f1': 0,
            'train_macro/precision': 0,
            'train_macro/recall': 0,
            'train_macro/f1': 0,
            'train_samples/precision': 0,
            'train_samples/recall': 0,
            'train_samples/f1': 0
        }
        model.train()
        train_loss = 0

        print(f'Train epoch: {epoch}')
        for labels, image in tqdm(train_data):
            optimizer.zero_grad()
            image = image.to(args.device)
            labels = labels.to(args.device)

            outputs = model(image)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
            metrics = calculate_metrics(outputs, labels)
            for metric in metrics:
                train_metrics[f'train_{metric}'] += metrics[metric]

            metrics['loss'] = loss.item()

            if args.wandb:
                wandb.log({f'train_iter_{k}':v for k, v in metrics.items()})
            
        train_epoch_metrics = {}
        for metric in train_metrics:
            train_epoch_metrics[metric.replace('iter', 'epoch')] = train_metrics[metric] / len(train_data)

        train_epoch_metrics['train_epoch_loss'] = train_loss / len(train_data)

        if args.wandb:
            wandb.log(train_epoch_metrics)
        
        model.eval()
        val_loss = 0
        val_metrics = {
            'val_micro/precision': 0,
            'val_micro/recall': 0,
            'val_micro/f1': 0,
            'val_macro/precision': 0,
            'val_macro/recall': 0,
            'val_macro/f1': 0,
            'val_samples/precision': 0,
            'val_samples/recall': 0,
            'val_samples/f1': 0
        }

        print('Val:')
        with torch.no_grad():
            for labels, image in tqdm(val_data):
                image = image.to(args.device)
                labels = labels.to(args.device)

                outputs = model(image)
                loss = criterion(outputs, labels)
                val_loss += loss.item()
                
                metrics = calculate_metrics(outputs, labels)

                for metric in metrics:
                    val_metrics[f'val_{metric}'] += metrics[metric]
                
                metrics['loss'] = loss.item()


                if args.wandb:
                    wandb.log({f'val_iter_{k}':v for k, v in metrics.items()})

        val_epoch_metrics = {}
        for metric in val_metrics:
            val_epoch_metrics[metric.replace('iter', 'epoch')] = val_metrics[metric] / len(val_data)
        
        val_epoch_metrics['val_epoch_loss'] = val_loss / len(val_data)

        if args.wandb:
            wandb.log(val_epoch_metrics)

        ckpt = {
            'train_loss': train_loss,
            'val_loss': val_loss,
            'state_dict': model.state_dict(),
            **val_epoch_metrics
        }
        torch.save(ckpt, os.path.join(args.save_dir, 'model.pth'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='retrieval model parameters')
    parser.add_argument('--seed', default=8, type=int)
    parser.add_argument('--wandb', default=1, type=int, choices=[0,1])
    parser.add_argument('--device', default='cuda:7', type=str)
    parser.add_argument('--workers', default=8, type=int)
    parser.add_argument('--batch_size', default=64, type=int)
    parser.add_argument('--epochs', default=250, type=int)
    parser.add_argument('--lr', default=0.0001, type=float)
    parser.add_argument('--lmdb_file', default=f'/common/home/as3503/as3503/courses/cs535/535/dataset/Pizza10/data.lmdb')
    parser.add_argument('--ckpt_path', default='')
    parser.add_argument('--image_encoder_weights', default=None)
    parser.add_argument('--debug', default=False, type=bool)
    parser.add_argument('--save_dir', default='/common/home/as3503/as3503/courses/cs535/535/IngredientClassifier/checkpoints/')


    args = parser.parse_args()


    if args.wandb:
        project_name = '535_ingredient_classifier'
        wandb.init(project=project_name, config=args)
        wandb.config.update(args)
        save_dir = os.path.join(os.path.dirname(__file__),  'runs_ingredient_classifier', wandb.run.id)

    
    train_dataset = Pizza10Dataset(part='train')
    val_dataset = Pizza10Dataset(part='val')
    model = IngredientClassifier().to(args.device)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, num_workers=args.workers, collate_fn=my_collate)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=args.batch_size, num_workers=args.workers, collate_fn=my_collate)
    train(model, train_loader, val_loader, args)