from io import BytesIO
import json
import lmdb
from PIL import Image
from torch.utils.data import Dataset
import os
import numpy as np

class MultiResolutionDataset_(Dataset):
    def __init__(self, path, transform, resolution=256):
        self.env = lmdb.open(
            path,
            max_readers=32,
            readonly=True,
            lock=False,
            readahead=False,
            meminit=False,
        )

        if not self.env:
            raise IOError('Cannot open lmdb dataset', path)

        with self.env.begin(write=False) as txn:
            self.length = int(txn.get('length'.encode('utf-8')).decode('utf-8'))

        self.resolution = resolution
        self.transform = transform

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        with self.env.begin(write=False) as txn:
            key = f'{self.resolution}-{str(index).zfill(5)}'.encode('utf-8')
            img_bytes = txn.get(key)

        buffer = BytesIO(img_bytes)
        img = Image.open(buffer)
        img = self.transform(img)

        return img

class MultiResolutionDataset(Dataset):
    def __init__(
        self, 
        path=f'', 
        transform=None,
        resolution=256,
        part='train', return_image=True):

        dirname = os.path.dirname(path)
        label_file = os.path.join(dirname, 'imageLabels.txt')
        with open(label_file, 'r') as f:
            self.labels = f.read().strip().split('\n')

        label_dict= {}
        for idx,line in enumerate(self.labels):
            if not line in label_dict:
                label_dict[line] = []
            label_dict[line].append(idx)
        
        items = label_dict.items()
        items = sorted(items, key=lambda x: -len(x[1]))
        
        filename = os.path.join(dirname, 'statistics.json')
        with open(filename, 'w') as f:
            json.dump(items, f, indent=2)
        
        self.label_list = list(label_dict.values())
        self.class_sample_count = [len(x) for x in self.label_list]
        
        self.env = lmdb.open(
            path,
            max_readers=32,
            readonly=True,
            lock=False,
            readahead=False,
            meminit=False,
        )

        if not self.env:
            raise IOError('Cannot open lmdb dataset', path)

        with self.env.begin(write=False) as txn:
            self.length = int(txn.get('length'.encode('utf-8')).decode('utf-8'))

        self.resolution = resolution

        assert transform!=None, 'transform can not be None!'
        self.transform = transform

        self.part = part

        self.return_image = return_image

    def __len__(self):
        return len(self.label_list)

    def _load_pizza(self, idx):
        with self.env.begin(write=False) as txn:
            key = f'{idx}'.encode('utf-8')
            ingrs = txn.get(key).decode('utf-8')
            if not ingrs:
                ingrs = 'empty'
            txt = ingrs

            key = f'{self.resolution}-{idx}'.encode('utf-8')
            img_bytes = txn.get(key)
        
        buffer = BytesIO(img_bytes)
        img = Image.open(buffer)
        img = self.transform(img)
        return txt, img

    def __getitem__(self, idx):
        pizzas = self.label_list[idx]
        if self.part=="train":
            index = pizzas[int(3*np.random.random()/5*len(pizzas))]
        else:
            index = pizzas[int(3*len(pizzas)/5)+int(2*np.random.random()*len(pizzas)/5)]
        txt, img = self._load_pizza(index)

        if not self.return_image:
            return txt

        return img