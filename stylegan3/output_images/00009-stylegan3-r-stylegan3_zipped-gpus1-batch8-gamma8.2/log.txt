Loading training set...

Num images:  18434
Image shape: [3, 1024, 1024]
Label shape: [0]

Constructing networks...
Setting up PyTorch plugin "bias_act_plugin"... Done.
Setting up PyTorch plugin "filtered_lrelu_plugin"... Done.

Generator                      Parameters  Buffers  Output shape          Datatype
---                            ---         ---      ---                   ---     
mapping.fc0                    262656      -        [8, 512]              float32 
mapping.fc1                    262656      -        [8, 512]              float32 
mapping                        -           512      [8, 16, 512]          float32 
synthesis.input.affine         2052        -        [8, 4]                float32 
synthesis.input                1048576     3081     [8, 1024, 36, 36]     float32 
synthesis.L0_36_1024.affine    525312      -        [8, 1024]             float32 
synthesis.L0_36_1024           1049600     157      [8, 1024, 36, 36]     float32 
synthesis.L1_36_1024.affine    525312      -        [8, 1024]             float32 
synthesis.L1_36_1024           1049600     157      [8, 1024, 36, 36]     float32 
synthesis.L2_52_1024.affine    525312      -        [8, 1024]             float32 
synthesis.L2_52_1024           1049600     169      [8, 1024, 52, 52]     float32 
synthesis.L3_52_1024.affine    525312      -        [8, 1024]             float32 
synthesis.L3_52_1024           1049600     157      [8, 1024, 52, 52]     float32 
synthesis.L4_84_1024.affine    525312      -        [8, 1024]             float32 
synthesis.L4_84_1024           1049600     169      [8, 1024, 84, 84]     float32 
synthesis.L5_148_1024.affine   525312      -        [8, 1024]             float32 
synthesis.L5_148_1024          1049600     169      [8, 1024, 148, 148]   float16 
synthesis.L6_148_1024.affine   525312      -        [8, 1024]             float32 
synthesis.L6_148_1024          1049600     157      [8, 1024, 148, 148]   float16 
synthesis.L7_276_645.affine    525312      -        [8, 1024]             float32 
synthesis.L7_276_645           661125      169      [8, 645, 276, 276]    float16 
synthesis.L8_276_406.affine    330885      -        [8, 645]              float32 
synthesis.L8_276_406           262276      157      [8, 406, 276, 276]    float16 
synthesis.L9_532_256.affine    208278      -        [8, 406]              float32 
synthesis.L9_532_256           104192      169      [8, 256, 532, 532]    float16 
synthesis.L10_1044_161.affine  131328      -        [8, 256]              float32 
synthesis.L10_1044_161         41377       169      [8, 161, 1044, 1044]  float16 
synthesis.L11_1044_102.affine  82593       -        [8, 161]              float32 
synthesis.L11_1044_102         16524       157      [8, 102, 1044, 1044]  float16 
synthesis.L12_1044_64.affine   52326       -        [8, 102]              float32 
synthesis.L12_1044_64          6592        25       [8, 64, 1044, 1044]   float16 
synthesis.L13_1024_64.affine   32832       -        [8, 64]               float32 
synthesis.L13_1024_64          4160        25       [8, 64, 1024, 1024]   float16 
synthesis.L14_1024_3.affine    32832       -        [8, 64]               float32 
synthesis.L14_1024_3           195         1        [8, 3, 1024, 1024]    float16 
synthesis                      -           -        [8, 3, 1024, 1024]    float32 
---                            ---         ---      ---                   ---     
Total                          15093151    5600     -                     -       

Setting up PyTorch plugin "upfirdn2d_plugin"... Done.

Discriminator  Parameters  Buffers  Output shape         Datatype
---            ---         ---      ---                  ---     
b1024.fromrgb  128         16       [8, 32, 1024, 1024]  float16 
b1024.skip     2048        16       [8, 64, 512, 512]    float16 
b1024.conv0    9248        16       [8, 32, 1024, 1024]  float16 
b1024.conv1    18496       16       [8, 64, 512, 512]    float16 
b1024          -           16       [8, 64, 512, 512]    float16 
b512.skip      8192        16       [8, 128, 256, 256]   float16 
b512.conv0     36928       16       [8, 64, 512, 512]    float16 
b512.conv1     73856       16       [8, 128, 256, 256]   float16 
b512           -           16       [8, 128, 256, 256]   float16 
b256.skip      32768       16       [8, 256, 128, 128]   float16 
b256.conv0     147584      16       [8, 128, 256, 256]   float16 
b256.conv1     295168      16       [8, 256, 128, 128]   float16 
b256           -           16       [8, 256, 128, 128]   float16 
b128.skip      131072      16       [8, 512, 64, 64]     float16 
b128.conv0     590080      16       [8, 256, 128, 128]   float16 
b128.conv1     1180160     16       [8, 512, 64, 64]     float16 
b128           -           16       [8, 512, 64, 64]     float16 
b64.skip       262144      16       [8, 512, 32, 32]     float32 
b64.conv0      2359808     16       [8, 512, 64, 64]     float32 
b64.conv1      2359808     16       [8, 512, 32, 32]     float32 
b64            -           16       [8, 512, 32, 32]     float32 
b32.skip       262144      16       [8, 512, 16, 16]     float32 
b32.conv0      2359808     16       [8, 512, 32, 32]     float32 
b32.conv1      2359808     16       [8, 512, 16, 16]     float32 
b32            -           16       [8, 512, 16, 16]     float32 
b16.skip       262144      16       [8, 512, 8, 8]       float32 
b16.conv0      2359808     16       [8, 512, 16, 16]     float32 
b16.conv1      2359808     16       [8, 512, 8, 8]       float32 
b16            -           16       [8, 512, 8, 8]       float32 
b8.skip        262144      16       [8, 512, 4, 4]       float32 
b8.conv0       2359808     16       [8, 512, 8, 8]       float32 
b8.conv1       2359808     16       [8, 512, 4, 4]       float32 
b8             -           16       [8, 512, 4, 4]       float32 
b4.mbstd       -           -        [8, 513, 4, 4]       float32 
b4.conv        2364416     16       [8, 512, 4, 4]       float32 
b4.fc          4194816     -        [8, 512]             float32 
b4.out         513         -        [8, 1]               float32 
---            ---         ---      ---                  ---     
Total          29012513    544      -                    -       

Setting up augmentation...
Distributing across 1 GPUs...
Setting up training phases...
Exporting sample images...
Initializing logs...
Training for 25000 kimg...

tick 0     kimg 0.0      time 4m 55s       sec/tick 10.2    sec/kimg 1278.20 maintenance 284.7  cpumem 4.92   gpumem 19.64  reserved 22.18  augment 0.000
Evaluating metrics...
{"results": {"fid50k_full": 481.8228154798764}, "metric": "fid50k_full", "total_time": 2898.65634560585, "total_time_str": "48m 19s", "num_gpus": 1, "snapshot_pkl": "network-snapshot-000000.pkl", "timestamp": 1637536815.9558845}
tick 1     kimg 4.0      time 1h 16m 13s   sec/tick 1371.0  sec/kimg 342.74  maintenance 2907.5 cpumem 6.52   gpumem 20.19  reserved 21.92  augment 0.005
tick 2     kimg 8.0      time 1h 39m 11s   sec/tick 1377.2  sec/kimg 344.30  maintenance 0.2    cpumem 6.52   gpumem 20.19  reserved 21.77  augment 0.011
tick 3     kimg 12.0     time 2h 02m 11s   sec/tick 1379.4  sec/kimg 344.86  maintenance 0.4    cpumem 6.52   gpumem 20.23  reserved 21.77  augment 0.016
tick 4     kimg 16.0     time 2h 25m 11s   sec/tick 1380.1  sec/kimg 345.04  maintenance 0.2    cpumem 6.52   gpumem 20.16  reserved 21.77  augment 0.021
tick 5     kimg 20.0     time 2h 48m 10s   sec/tick 1379.0  sec/kimg 344.76  maintenance 0.4    cpumem 6.52   gpumem 20.31  reserved 21.77  augment 0.025
tick 6     kimg 24.0     time 3h 11m 16s   sec/tick 1385.5  sec/kimg 346.36  maintenance 0.4    cpumem 6.52   gpumem 20.26  reserved 21.77  augment 0.029
tick 7     kimg 28.0     time 3h 34m 25s   sec/tick 1388.0  sec/kimg 347.01  maintenance 0.2    cpumem 6.52   gpumem 20.80  reserved 21.77  augment 0.033
tick 8     kimg 32.0     time 3h 57m 32s   sec/tick 1387.1  sec/kimg 346.76  maintenance 0.4    cpumem 6.52   gpumem 20.12  reserved 21.77  augment 0.036
tick 9     kimg 36.0     time 4h 20m 31s   sec/tick 1378.5  sec/kimg 344.63  maintenance 0.4    cpumem 6.52   gpumem 20.24  reserved 21.77  augment 0.041
tick 10    kimg 40.0     time 4h 43m 29s   sec/tick 1377.4  sec/kimg 344.35  maintenance 0.3    cpumem 6.52   gpumem 20.44  reserved 21.77  augment 0.046
tick 11    kimg 44.0     time 5h 06m 29s   sec/tick 1380.0  sec/kimg 345.00  maintenance 0.2    cpumem 6.52   gpumem 20.29  reserved 21.77  augment 0.050
tick 12    kimg 48.0     time 5h 29m 35s   sec/tick 1385.5  sec/kimg 346.37  maintenance 0.4    cpumem 6.52   gpumem 20.20  reserved 21.77  augment 0.055
tick 13    kimg 52.0     time 5h 52m 40s   sec/tick 1385.0  sec/kimg 346.26  maintenance 0.4    cpumem 6.52   gpumem 20.31  reserved 21.77  augment 0.061
tick 14    kimg 56.0     time 6h 15m 41s   sec/tick 1380.9  sec/kimg 345.21  maintenance 0.4    cpumem 6.52   gpumem 20.45  reserved 21.77  augment 0.066
tick 15    kimg 60.0     time 6h 38m 40s   sec/tick 1378.3  sec/kimg 344.57  maintenance 0.2    cpumem 6.52   gpumem 20.18  reserved 21.77  augment 0.072
tick 16    kimg 64.0     time 7h 01m 39s   sec/tick 1379.1  sec/kimg 344.78  maintenance 0.2    cpumem 6.52   gpumem 20.64  reserved 21.77  augment 0.078
tick 17    kimg 68.0     time 7h 24m 44s   sec/tick 1384.5  sec/kimg 346.13  maintenance 0.4    cpumem 6.53   gpumem 20.41  reserved 21.77  augment 0.082
tick 18    kimg 72.0     time 7h 47m 52s   sec/tick 1387.0  sec/kimg 346.76  maintenance 0.2    cpumem 6.53   gpumem 20.66  reserved 21.77  augment 0.086
tick 19    kimg 76.0     time 8h 10m 58s   sec/tick 1386.0  sec/kimg 346.51  maintenance 0.4    cpumem 6.53   gpumem 20.27  reserved 21.77  augment 0.092
tick 20    kimg 80.0     time 8h 34m 00s   sec/tick 1381.6  sec/kimg 345.40  maintenance 0.4    cpumem 6.53   gpumem 20.37  reserved 21.77  augment 0.097
tick 21    kimg 84.0     time 8h 57m 00s   sec/tick 1379.7  sec/kimg 344.93  maintenance 0.4    cpumem 6.53   gpumem 20.60  reserved 21.77  augment 0.103
tick 22    kimg 88.0     time 9h 20m 04s   sec/tick 1384.0  sec/kimg 346.01  maintenance 0.4    cpumem 6.53   gpumem 20.69  reserved 21.77  augment 0.108
tick 23    kimg 92.0     time 9h 43m 12s   sec/tick 1387.6  sec/kimg 346.90  maintenance 0.2    cpumem 6.53   gpumem 20.50  reserved 21.77  augment 0.114
tick 24    kimg 96.0     time 10h 06m 21s  sec/tick 1388.5  sec/kimg 347.13  maintenance 0.2    cpumem 6.53   gpumem 20.46  reserved 21.77  augment 0.119
tick 25    kimg 100.0    time 10h 29m 24s  sec/tick 1382.2  sec/kimg 345.54  maintenance 0.4    cpumem 6.53   gpumem 20.66  reserved 21.77  augment 0.125
tick 26    kimg 104.0    time 10h 52m 25s  sec/tick 1381.1  sec/kimg 345.28  maintenance 0.4    cpumem 6.53   gpumem 20.43  reserved 21.77  augment 0.130
Traceback (most recent call last):
  File "/common/users/as3503/courses/cs535/535/stylegan3/train.py", line 286, in <module>
    main() # pylint: disable=no-value-for-parameter
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1128, in __call__
    return self.main(*args, **kwargs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1053, in main
    rv = self.invoke(ctx)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 1395, in invoke
    return ctx.invoke(self.callback, **ctx.params)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/click/core.py", line 754, in invoke
    return __callback(*args, **kwargs)
  File "/common/users/as3503/courses/cs535/535/stylegan3/train.py", line 281, in main
    launch_training(c=c, desc=desc, outdir=opts.outdir, dry_run=opts.dry_run)
  File "/common/users/as3503/courses/cs535/535/stylegan3/train.py", line 96, in launch_training
    subprocess_fn(rank=0, c=c, temp_dir=temp_dir)
  File "/common/users/as3503/courses/cs535/535/stylegan3/train.py", line 47, in subprocess_fn
    training_loop.training_loop(rank=rank, **c)
  File "/common/users/as3503/courses/cs535/535/stylegan3/training/training_loop.py", line 280, in training_loop
    loss.accumulate_gradients(phase=phase.name, real_img=real_img, real_c=real_c, gen_z=gen_z, gen_c=gen_c, gain=phase.interval, cur_nimg=cur_nimg)
  File "/common/users/as3503/courses/cs535/535/stylegan3/training/loss.py", line 81, in accumulate_gradients
    loss_Gmain.mean().mul(gain).backward()
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/_tensor.py", line 255, in backward
    torch.autograd.backward(self, gradient, retain_graph, create_graph, inputs=inputs)
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/autograd/__init__.py", line 147, in backward
    Variable._execution_engine.run_backward(
  File "/common/home/as3503/.conda/envs/stylegan3/lib/python3.9/site-packages/torch/autograd/function.py", line 87, in apply
    return self._forward_cls.backward(self, *args)  # type: ignore[attr-defined]
  File "/common/users/as3503/courses/cs535/535/stylegan3/torch_utils/ops/conv2d_gradfix.py", line 140, in backward
    grad_input = op.apply(grad_output, weight, None)
  File "/common/users/as3503/courses/cs535/535/stylegan3/torch_utils/ops/conv2d_gradfix.py", line 126, in forward
    return torch.nn.functional.conv_transpose2d(input=input, weight=weight, bias=bias, output_padding=output_padding, **common_kwargs)
RuntimeError: CUDA out of memory. Tried to allocate 514.00 MiB (GPU 0; 23.69 GiB total capacity; 19.94 GiB already allocated; 513.50 MiB free; 21.76 GiB reserved in total by PyTorch)
